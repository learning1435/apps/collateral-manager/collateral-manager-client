import {Injectable} from '@angular/core';
import {HttpService} from '../services/http-client.service';
import {Observable} from 'rxjs';
import {CollateralKind} from '../../@models';

@Injectable({providedIn: 'root'})
export class TagsService {

  constructor(
    private _http: HttpService,
  ) {
  }

  public getList(): Observable<CollateralKind[]> {
    return this._http.get('/api/v1/tags');
  }

  public add(payload: any): Observable<CollateralKind> {
    return this._http.post('/api/v1/tags', payload);
  }

  public update(id: number, payload: any): Observable<CollateralKind> {
    return this._http.put(`api/v1/tags/${id}`, payload);
  }

  public remove(id: number): Observable<void> {
    return this._http.delete(`api/v1/tags/${id}`);
  }
}
