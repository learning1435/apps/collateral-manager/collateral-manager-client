import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Collateral} from '../../@models';
import {HttpService} from '../services/http-client.service';

@Injectable({providedIn: 'root'})
export class CollateralsService {

  constructor(
    private _http: HttpService,
  ) {
  }

  public getList(): Observable<Collateral[]> {
    return this._http.get('api/v1/collaterals');
  }

  public add(payload: any): Observable<Collateral> {
    return this._http.post('api/v1/collaterals', payload);
  }

  public update(id: number, payload: any): Observable<Collateral> {
    return this._http.put(`api/v1/collaterals/${id}`, payload);
  }

  public remove(id: number): Observable<void> {
    return this._http.delete(`api/v1/collaterals/${id}`);
  }
}
