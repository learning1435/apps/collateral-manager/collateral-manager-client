import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {NbAuthService} from '@nebular/auth';
import {Observable, of, Subscriber} from 'rxjs';
import {take} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class IsAuthenticatedGuard implements CanActivate {

  constructor(
    private _nbAuthService: NbAuthService,
    private _router: Router,
  ) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return of(true);
    // return new Observable<boolean>((subscriber: Subscriber<boolean>) => {
    //   this._nbAuthService.isAuthenticatedOrRefresh()
    //     .pipe(take(1))
    //     .subscribe((isAuth: boolean) => {
    //       if (isAuth) {
    //         subscriber.next(true);
    //         subscriber.complete();
    //       } else {
    //         this._router.navigate(['/', 'auth', 'login'])
    //           .then(() => {
    //             subscriber.next(false);
    //             subscriber.complete();
    //           });
    //       }
    //     });
    // });
  }
}
