import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';

@Injectable()
export class HttpService {

  constructor(private httpClient: HttpClient) {
  }

  public get<T>(url: string, queryParams?: HttpParams): Observable<T> {
    return this.httpClient.get<T>(this._getUrl(url), {params: queryParams});
  }

  public post<T>(url: string, data: any): Observable<T> {
    return this.httpClient.post<T>(this._getUrl(url), data, {
      headers: {'Content-Type': 'application/json;charset=UTF-8'},
    });
  }

  public put<T>(url: string, data: any): Observable<T> {
    return this.httpClient.put<T>(this._getUrl(url), data, {
      headers: {'Content-Type': 'application/json;charset=UTF-8'},
    });
  }

  public delete(url: string, data: any = null): Observable<any> {
    const httpOptions = {};
    if (data) {
      httpOptions['params'] = data;
    }

    return this.httpClient.delete(this._getUrl(url), httpOptions);
  }

  private _getUrl(url: string): string {
    return `${environment.baseUrl}/${url}`;
  }
}
