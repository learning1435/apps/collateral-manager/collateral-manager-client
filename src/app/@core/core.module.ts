import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MAT_RIPPLE_GLOBAL_OPTIONS} from '@angular/material/core';
import {NbAuthModule,  NbPasswordAuthStrategy} from '@nebular/auth';
import {NbSecurityModule, NbRoleProvider} from '@nebular/security';
import {Observable, of as observableOf} from 'rxjs';

import {
  AnalyticsService,
  LayoutService,
  PlayerService,
  SeoService,
  StateService,
  RippleService,
} from './utils';

import {
   IsAuthenticatedGuard,
} from './guards';
import {CollateralKindsService} from 'app/@core/api/collateral-kinds.service';
import {TagsService} from 'app/@core/api/tags.service';
import {HttpService} from 'app/@core/services/http-client.service';

const DATA_SERVICES = [
  // {provide: UserData, useClass: UserService},
  {provide: MAT_RIPPLE_GLOBAL_OPTIONS, useExisting: RippleService},
  HttpService,
  CollateralKindsService,
  TagsService,
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  public getRole(): Observable<string> {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const NB_CORE_PROVIDERS = [
  ...DATA_SERVICES,
  ...NbAuthModule.forRoot({

    strategies: [
      NbPasswordAuthStrategy.setup({
        name: 'email',
        baseEndpoint: '/auth',
        login: {
          method: 'POST',
          endpoint: 'authenticate',
          redirect: {
            success: '/',
          },
        },
      }),
    ],
  }).providers,

  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,

  {
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
  },
  AnalyticsService,
  LayoutService,
  PlayerService,
  SeoService,
  StateService,
  IsAuthenticatedGuard,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
}
