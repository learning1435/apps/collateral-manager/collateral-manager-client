// import {Injectable, Injector} from '@angular/core';
// import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
// import {NbAuthJWTToken, NbAuthService, NbAuthToken, NbTokenService} from '@nebular/auth';
// import {TranslateService} from '@ngx-translate/core';
// import {Observable, of, throwError} from 'rxjs';
// import {catchError, switchMap, tap} from 'rxjs/operators';
// import {ToastService} from '../../shared/services/toast.service';
// import {RouterService} from '../utils';
// import {AuthRoutingUrl} from '../../auth/auth-routing.url';
// import {AuthService} from '../../shared/services/api/auth/auth.service';
// import {AppConfig} from '../../app.config';
// import {AppConstants} from '../app.constants';
//
// @Injectable()
// export class AuthInterceptor implements HttpInterceptor {
//
//   constructor(
//     private injector: Injector,
//     private config: AppConfig,
//     private translate: TranslateService,
//     private toastService: ToastService,
//     private routerService: RouterService,
//     private authService: AuthService,
//     private tokenService: NbTokenService,
//   ) {
//   }
//
//   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     if (req.url.includes('/api/login')) {
//       return next.handle(req)
//         .pipe(
//           tap((response: any) => {
//             if (response.body && response.body.refresh_token) {
//               localStorage.setItem(this.getLocalStorageRefreshTokenKey(), response.body.refresh_token);
//             }
//           }),
//         );
//     }
//
//     if (req.url.includes('/api/token/refresh')) {
//       req = req.clone({
//         body: {refresh_token: localStorage.getItem(this.getLocalStorageRefreshTokenKey())},
//       });
//
//       return next.handle(req)
//         .pipe(
//           catchError((httpErrorResponse: HttpErrorResponse) => {
//             this.logout();
//             return throwError(httpErrorResponse);
//           }),
//         );
//     }
//
//     if (req.url.includes('/logout')) {
//       this.logout();
//       return of();
//     }
//
//     return this.nbAuthService.isAuthenticated()
//       .pipe(
//         switchMap(authenticated => {
//           if (authenticated) {
//             return this.doRequest(req, next);
//           } else {
//             return this.refreshToken(req, next);
//           }
//         }),
//       );
//   }
//
//   private doRequest(req, next): Observable<any> {
//     return this.nbAuthService.getToken()
//       .pipe(
//         switchMap((token: NbAuthToken) => {
//           const JWT = `Bearer ${token.getValue()}`;
//
//           req = req.clone({
//             setHeaders: {Authorization: JWT},
//           });
//
//           return next.handle(req)
//             .pipe(
//               catchError((httpErrorResponse: HttpErrorResponse) => {
//                 if (this.isUnauthorizedResponse(httpErrorResponse)) {
//                   return this.refreshToken(req, next);
//                 }
//
//                 return throwError(httpErrorResponse);
//               }),
//             );
//         }),
//       );
//   }
//
//   private refreshToken(req, next): Observable<any> {
//     return this.authService.refreshToken()
//       .pipe(
//         switchMap((response) => {
//           this.handleNewToken(response.token, response.refresh_token);
//           return this.doRequest(req, next);
//         }),
//       );
//   }
//
//   private logout(): void {
//     this.nbAuthService.logout(AppConstants.AUTH_STRATEGY);
//     this.tokenService.clear();
//     localStorage.removeItem(this.getLocalStorageRefreshTokenKey());
//     this.routerService.navigate(AuthRoutingUrl.LOGIN);
//   }
//
//   // noinspection JSMethodCanBeStatic
//   private isUnauthorizedResponse(httpErrorResponse: HttpErrorResponse): boolean {
//     return httpErrorResponse.status === 401;
//   }
//
//   private handleNewToken(token: string, refreshToken: string): void {
//     const nbAuthJWTToken = new NbAuthJWTToken(token, AppConstants.AUTH_STRATEGY);
//     this.tokenService.set(nbAuthJWTToken);
//     localStorage.setItem(this.getLocalStorageRefreshTokenKey(), refreshToken);
//   }
//
//   private get nbAuthService(): NbAuthService {
//     return this.injector.get(NbAuthService);
//   }
//
//   private getLocalStorageRefreshTokenKey(): string {
//     return this.config.getValue(AppConstants.CONFIG_LOCAL_STORAGE_REFRESH_TOKEN);
//   }
// }
