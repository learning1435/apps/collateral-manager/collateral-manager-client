// import {Injectable} from '@angular/core';
// import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
// import {TranslateService} from '@ngx-translate/core';
// import {throwError as observableThrowError, Observable} from 'rxjs';
// import {catchError} from 'rxjs/operators';
// import {ToastService} from '../../shared/services/toast.service';
// import {AppConfig} from '../../app.config';
// import {AppConstants} from '../app.constants';
// import {ErrorResponseMessage, ErrorMessageTranslation, ValidationError} from '../../shared/models/common';
//
// interface ErrorResponse {
//   code: number;
//   message: string;
//   messageParams: any;
//   validationErrors?: ValidationError[];
// }
//
// @Injectable()
// export class ErrorResponseInterceptor implements HttpInterceptor {
//
//   private hideErrorMessageExceptions: number[] = [
//     AppConstants.ERROR_VALIDATION_CODE,
//     AppConstants.ERROR_CURRENT_PACKET_NOT_FOUND,
//   ];
//
//   constructor(
//     private config: AppConfig,
//     private translate: TranslateService,
//     private toastService: ToastService,
//   ) {
//   }
//
//   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     return next.handle(req)
//       .pipe(
//         catchError((httpErrorResponse: HttpErrorResponse) => {
//           const errorResponseMessage = this.handleError(httpErrorResponse);
//           return observableThrowError(errorResponseMessage);
//         }),
//       );
//   }
//
//   private handleError(httpErrorResponse: HttpErrorResponse): ErrorResponseMessage {
//     const errorResponseMessage: ErrorResponseMessage = this.getErrorResponseMessage(httpErrorResponse);
//     const errorTitle = this.translate.instant('COMMON.ERROR');
//
//     if (!this.hideErrorMessageExceptions.includes(errorResponseMessage.errorCode)) {
//       this.toastService.showDangerToast(errorTitle, errorResponseMessage.errorMessage);
//     }
//
//     return errorResponseMessage;
//   }
//
//   private getErrorResponseMessage(httpErrorResponse: HttpErrorResponse): ErrorResponseMessage {
//     const errorContentType = httpErrorResponse.headers.get('content-type');
//     let error: ErrorResponse = httpErrorResponse.error;
//     let errorCode = AppConstants.ERROR_DEFAULT_CODE;
//     let errorMessage = this.translate.instant(`ERROR_CODES.${errorCode}.message`);
//     let errorMessageParams = null;
//     let validationErrors: ValidationError[] = [];
//
//     if (typeof error === 'string' && errorContentType.includes('application/json')) {
//       error = JSON.parse(error);
//     }
//
//     if (error.hasOwnProperty('code')) {
//       errorCode = error.code;
//     }
//
//     if (error.hasOwnProperty('messageParams') && typeof error.messageParams === 'object') {
//       errorMessageParams = error.messageParams;
//     }
//
//     if (error.hasOwnProperty('validationErrors')) {
//       validationErrors = error.validationErrors;
//     }
//
//     const errorCodes: any = this.translate.instant('ERROR_CODES');
//
//     if (errorCodes.hasOwnProperty(errorCode)) {
//       errorMessage = this.getErrorMessage(errorCodes, errorCode, errorMessageParams);
//     }
//
//     return {errorMessage, errorCode, validationErrors};
//   }
//
//   private getErrorMessage(errorCodes: object, errorCode: number, params: object): string {
//     const errorCodeTranslation: ErrorMessageTranslation = errorCodes[errorCode];
//
//     if (errorCodeTranslation.hasOwnProperty('template') && params) {
//       return this.translate.instant(`ERROR_CODES.${errorCode}.template`, params);
//     } else {
//       return this.translate.instant(`ERROR_CODES.${errorCode}.message`);
//     }
//   }
// }
