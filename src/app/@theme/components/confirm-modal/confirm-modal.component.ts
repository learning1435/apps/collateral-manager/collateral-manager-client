import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
})
export class ConfirmModalComponent {

  @Input() title: string;
  @Input() message: string;

  constructor(
    private _ref: NbDialogRef<ConfirmModalComponent>,
  ) {
  }

  public confirm(): void {
    this._ref.close(true);
  }

  public dismiss(): void {
    this._ref.close(false);
  }
}
