import {CollateralKind} from './collateral-kind';
import {Tag} from './tag';

export interface Collateral {
  id: number;
  name: string;
  url: string | null;
  kind: CollateralKind;
  tags: Tag[];
}
