import {Tag} from './tag';

export class CollateralTag extends Tag {
  weight: number;
}
