export class CollateralKind {
  id: number;
  name: string;
  color: string;
}
