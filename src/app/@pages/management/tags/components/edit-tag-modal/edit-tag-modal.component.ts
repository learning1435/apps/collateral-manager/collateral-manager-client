import {Component, Input, OnInit} from '@angular/core';
import {Tag} from '../../../../../@models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {TagsService} from '../../../../../@core/api';

@Component({
  selector: 'app-edit-tag-modal',
  templateUrl: './edit-tag-modal.component.html',
  styleUrls: ['./edit-tag-modal.component.scss'],
})
export class EditTagModalComponent implements OnInit {

  @Input() item: Tag;

  public form: FormGroup;

  constructor(
    private _ref: NbDialogRef<EditTagModalComponent>,
    private _tagsService: TagsService,
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(100),
      ]),
      color: new FormControl('#ffffff', [
        Validators.required,
      ]),
    });
  }

  public ngOnInit(): void {
    if (this.item) {
      this.form.controls.name.setValue(this.item.name, {emitEvent: true});
      this.form.controls.color.setValue(this.item.color, {emitEvent: true});
    }
  }

  public save(): void {
    if (!this.form.valid) {
      return;
    }

    const data: any = {
      data: {
        attributes: {
          name: this.form.controls.name.value,
          color: this.form.controls.color.value,
        },
      },
    };

    this._tagsService
      .update(this.item.id, data)
      .subscribe((item: Tag) => {
        this._ref.close(item);
      });
  }

  public close(): void {
    this._ref.close(null);
  }
}
