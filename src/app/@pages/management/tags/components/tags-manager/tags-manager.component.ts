import {Component, OnDestroy, OnInit} from '@angular/core';
import {Tag} from '../../../../../@models';
import {Subject} from 'rxjs';
import {TagsService} from '../../../../../@core/api';
import {NbDialogService} from '@nebular/theme';
import {takeUntil} from 'rxjs/operators';
import {ConfirmModalComponent} from '../../../../../@theme/components/confirm-modal/confirm-modal.component';
import {CreateTagModalComponent} from '../create-tag-modal/create-tag-modal.component';
import {EditTagModalComponent} from '../edit-tag-modal/edit-tag-modal.component';

@Component({
  selector: 'app-tags-manager',
  templateUrl: './tags-manager.component.html',
  styleUrls: ['./tags-manager.component.scss'],
})
export class TagsManagerComponent  implements OnInit, OnDestroy {

  public tags: Tag[] = [];
  public loading: boolean = true;

  private _destroy$: Subject<void> = new Subject<void>();

  constructor(
    private _tagsService: TagsService,
    private _dialogService: NbDialogService,
  ) {
  }

  public ngOnInit(): void {
    this._tagsService.getList()
      .pipe(takeUntil(this._destroy$))
      .subscribe((list: Tag[]) => {
        this.tags = list;
        this.loading = false;
      });
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public add(): void {
    this._dialogService.open(CreateTagModalComponent, {
      context: {},
    })
      .onClose
      .subscribe((item: Tag | null) => {
        if (item) {
          this.tags.push(item);
        }
      });
  }

  public edit(index: number): void {
    this._dialogService.open(EditTagModalComponent, {
      context: {
        item: Object.assign({}, this.tags[index]),
      },
    })
      .onClose
      .subscribe((item: Tag | null) => {
        if (item) {
          this.tags[index] = item;
        }
      });
  }

  public remove(index: number): void {
    this._dialogService.open(ConfirmModalComponent, {
      context: {
        title: 'Remove collateral kind',
        message: 'Are you sure you want to remove this collateral kind?',
      },
    })
      .onClose
      .subscribe((confirm: boolean) => {
        if (confirm === true) {
          this._tagsService
            .remove(this.tags[index].id)
            .subscribe(() => {
              this.tags.splice(index, 1);
            });
        }
      });
  }
}
