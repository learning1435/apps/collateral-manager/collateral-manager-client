import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {TagsService} from '../../../../../@core/api';
import {Tag} from '../../../../../@models';

@Component({
  selector: 'app-create-tag-modal',
  templateUrl: './create-tag-modal.component.html',
  styleUrls: ['./create-tag-modal.component.scss'],
})
export class CreateTagModalComponent {

  public form: FormGroup;

  constructor(
    private _ref: NbDialogRef<CreateTagModalComponent>,
    private _tagsService: TagsService,
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(100),
      ]),
      color: new FormControl('#ffffff', [
        Validators.required,
      ]),
    });
  }

  public add(): void {
    if (!this.form.valid) {
      return;
    }

    const data: any = {
      data: {
        attributes: {
          name: this.form.controls.name.value,
          color: this.form.controls.color.value,
        },
      },
    };

    this._tagsService
      .add(data)
      .subscribe((item: Tag) => {
        this._ref.close(item);
      });
  }

  public close(): void {
    this._ref.close(null);
  }
}
