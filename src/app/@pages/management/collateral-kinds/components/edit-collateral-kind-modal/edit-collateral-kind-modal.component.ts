import {Component, Input, OnInit} from '@angular/core';
import {CollateralKind} from '../../../../../@models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {CollateralKindsService} from '../../../../../@core/api';

@Component({
  selector: 'app-edi-collateral-kind-modal',
  templateUrl: './edit-collateral-kind-modal.component.html',
  styleUrls: ['./edit-collateral-kind-modal.component.scss'],
})
export class EditCollateralKindModalComponent implements OnInit {

  @Input() item: CollateralKind;

  public form: FormGroup;

  constructor(
    private _ref: NbDialogRef<EditCollateralKindModalComponent>,
    private _collateralKindsService: CollateralKindsService,
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(100),
      ]),
      color: new FormControl('#ffffff', [
        Validators.required,
      ]),
    });
  }

  public ngOnInit(): void {
    if (this.item) {
      this.form.controls.name.setValue(this.item.name, {emitEvent: true});
      this.form.controls.color.setValue(this.item.color, {emitEvent: true});
    }
  }

  public save(): void {
    if (!this.form.valid) {
      return;
    }

    const data: any = {
      data: {
        attributes: {
          name: this.form.controls.name.value,
          color: this.form.controls.color.value,
        },
      },
    };

    this._collateralKindsService
      .update(this.item.id, data)
      .subscribe((item: CollateralKind) => {
        this._ref.close(item);
      });
  }

  public close(): void {
    this._ref.close(null);
  }
}
