import {Component} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {CollateralKindsService} from '../../../../../@core/api';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CollateralKind} from '../../../../../@models';

@Component({
  selector: 'app-create-collateral-kind-modal',
  templateUrl: './create-collateral-kind-modal.component.html',
  styleUrls: ['./create-collateral-kind-modal.component.scss'],
})
export class CreateCollateralKindModalComponent {

  public form: FormGroup;

  constructor(
    private _ref: NbDialogRef<CreateCollateralKindModalComponent>,
    private _collateralKindsService: CollateralKindsService,
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(100),
      ]),
      color: new FormControl('#ffffff', [
        Validators.required,
      ]),
    });
  }

  public add(): void {
    if (!this.form.valid) {
      return;
    }

    const data: any = {
      data: {
        attributes: {
          name: this.form.controls.name.value,
          color: this.form.controls.color.value,
        },
      },
    };

    this._collateralKindsService
      .add(data)
      .subscribe((item: CollateralKind) => {
        this._ref.close(item);
      });
  }

  public close(): void {
    this._ref.close(null);
  }
}
