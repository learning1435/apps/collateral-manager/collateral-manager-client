import {Component, OnDestroy, OnInit} from '@angular/core';
import {CollateralKindsService} from '../../../../../@core/api';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {CollateralKind} from '../../../../../@models';
import {NbDialogService} from '@nebular/theme';
import {CreateCollateralKindModalComponent} from '../create-collateral-kind-modal/create-collateral-kind-modal.component';
import {EditCollateralKindModalComponent} from '../edit-collateral-kind-modal/edit-collateral-kind-modal.component';
import {ConfirmModalComponent} from '../../../../../@theme/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-collateral-kinds-manager',
  templateUrl: './collateral-kinds-manager.component.html',
  styleUrls: ['./collateral-kinds-manager.component.scss'],
})
export class CollateralKindsManagerComponent implements OnInit, OnDestroy {

  public kinds: CollateralKind[] = [];
  public loading: boolean = true;

  private _destroy$: Subject<void> = new Subject<void>();

  constructor(
    private _collateralKindsService: CollateralKindsService,
    private _dialogService: NbDialogService,
  ) {
  }

  public ngOnInit(): void {
    this._collateralKindsService.getList()
      .pipe(takeUntil(this._destroy$))
      .subscribe((list: CollateralKind[]) => {
        this.kinds = list;
        this.loading = false;
      });
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public add(): void {
    this._dialogService.open(CreateCollateralKindModalComponent, {
      context: {},
    })
      .onClose
      .subscribe((item: CollateralKind | null) => {
        if (item) {
          this.kinds.push(item);
        }
      });
  }

  public edit(index: number): void {
    this._dialogService.open(EditCollateralKindModalComponent, {
      context: {
        item: Object.assign({}, this.kinds[index]),
      },
    })
      .onClose
      .subscribe((item: CollateralKind | null) => {
        if (item) {
          this.kinds[index] = item;
        }
      });
  }

  public remove(index: number): void {
    this._dialogService.open(ConfirmModalComponent, {
      context: {
        title: 'Remove collateral kind',
        message: 'Are you sure you want to remove this collateral kind?',
      },
    })
      .onClose
      .subscribe((confirm: boolean) => {
        if (confirm === true) {
          this._collateralKindsService
            .remove(this.kinds[index].id)
            .subscribe(() => {
              this.kinds.splice(index, 1);
            });
        }
      });
  }
}
