import {Component, Input, OnInit} from '@angular/core';
import {Collateral} from '../../../../@models';

@Component({
  selector: 'app-edit-collateral-modal',
  templateUrl: './edit-collateral-modal.component.html',
  styleUrls: ['./edit-collateral-modal.component.scss'],
})
export class EditCollateralModalComponent implements OnInit {

  @Input() item: Collateral;

  constructor() { }

  ngOnInit(): void {
  }

}
