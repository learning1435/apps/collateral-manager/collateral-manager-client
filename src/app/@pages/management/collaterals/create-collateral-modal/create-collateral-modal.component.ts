import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {CollateralsService, TagsService} from '../../../../@core/api';
import {Collateral, Tag} from '../../../../@models';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

export interface TagItem {
  tag: Tag;
  position: number;
}

@Component({
  selector: 'app-create-collateral-modal',
  templateUrl: './create-collateral-modal.component.html',
  styleUrls: ['./create-collateral-modal.component.scss'],
})
export class CreateCollateralModalComponent implements OnInit {

  public form: FormGroup;
  public tags: Tag[] = [];
  public tagItems: TagItem[];

  private _destroy: Subject<void> = new Subject<void>();

  constructor(
    private _ref: NbDialogRef<CreateCollateralModalComponent>,
    private _collateralsService: CollateralsService,
    private _tagsService: TagsService,
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(100),
      ]),
      url: new FormControl('#ffffff', [
        Validators.required,
      ]),
    });
  }

  public ngOnInit(): void {
    this._tagsService.getList()
      .pipe(takeUntil(this._destroy))
      .subscribe((tags: Tag[]) => {
        this.tags = tags;
      });
  }

  public assertTag(tag: Tag): void {
    this.tagItems.push({
      tag: tag,
      position: this.tagItems.length,
    });
  }

  public add(): void {
    if (!this.form.valid) {
      return;
    }

    const data: any = {
      data: {
        attributes: {
          name: this.form.controls.name.value,
          color: this.form.controls.color.value,
          tags: this.tagItems.map((tagItem: TagItem) => tagItem.tag.id),
        },
      },
    };

    this._collateralsService
      .add(data)
      .subscribe((item: Collateral) => {
        this._ref.close(item);
      });
  }

  public close(): void {
    this._ref.close(null);
  }
}
