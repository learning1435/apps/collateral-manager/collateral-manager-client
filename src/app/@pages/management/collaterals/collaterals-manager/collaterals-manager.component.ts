import {Component, OnDestroy, OnInit} from '@angular/core';
import {Collateral} from '../../../../@models';
import {Subject} from 'rxjs';
import {CollateralsService} from '../../../../@core/api';
import {NbDialogService} from '@nebular/theme';
import {takeUntil} from 'rxjs/operators';
import {ConfirmModalComponent} from '../../../../@theme/components/confirm-modal/confirm-modal.component';
import {CreateCollateralModalComponent} from '../create-collateral-modal/create-collateral-modal.component';
import {EditCollateralModalComponent} from '../edit-collateral-modal/edit-collateral-modal.component';

@Component({
  selector: 'app-collaterals-manager',
  templateUrl: './collaterals-manager.component.html',
  styleUrls: ['./collaterals-manager.component.scss'],
})
export class CollateralsManagerComponent implements OnInit, OnDestroy {

  public collaterals: Collateral[] = [];
  public loading: boolean = true;

  private _destroy$: Subject<void> = new Subject<void>();

  constructor(
    private _collateralsService: CollateralsService,
    private _dialogService: NbDialogService,
  ) {
  }

  public ngOnInit(): void {
    this._collateralsService.getList()
      .pipe(takeUntil(this._destroy$))
      .subscribe((list: Collateral[]) => {
        this.collaterals = list;
        this.loading = false;
      });
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public add(): void {
    this._dialogService.open(CreateCollateralModalComponent, {
      context: {},
    })
      .onClose
      .subscribe((item: Collateral | null) => {
        if (item) {
          this.collaterals.push(item);
        }
      });
  }

  public edit(index: number): void {
    this._dialogService.open(EditCollateralModalComponent, {
      context: {
        item: Object.assign({}, this.collaterals[index]),
      },
    })
      .onClose
      .subscribe((item: Collateral | null) => {
        if (item) {
          this.collaterals[index] = item;
        }
      });
  }

  public remove(index: number): void {
    this._dialogService.open(ConfirmModalComponent, {
      context: {
        title: 'Remove collateral',
        message: 'Are you sure you want to remove this collateral kind?',
      },
    })
      .onClose
      .subscribe((confirm: boolean) => {
        if (confirm === true) {
          this._collateralsService
            .remove(this.collaterals[index].id)
            .subscribe(() => {
              this.collaterals.splice(index, 1);
            });
        }
      });
  }
}
