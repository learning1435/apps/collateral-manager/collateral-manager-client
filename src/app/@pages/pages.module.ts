import {NgModule} from '@angular/core';
import {NbButtonModule, NbCardModule, NbInputModule, NbMenuModule, NbSpinnerModule} from '@nebular/theme';
import {ThemeModule} from '../@theme/theme.module';
import {PagesComponent} from './pages.component';
import {DashboardModule} from './dashboard/dashboard.module';
import {PagesRoutingModule} from './pages-routing.module';
import {MiscellaneousModule} from './miscellaneous/miscellaneous.module';
import {CollateralKindsManagerComponent} from './management/collateral-kinds/components/collateral-kinds-manager/collateral-kinds-manager.component';
import {CreateCollateralKindModalComponent} from './management/collateral-kinds/components/create-collateral-kind-modal/create-collateral-kind-modal.component';
import {EditCollateralKindModalComponent} from './management/collateral-kinds/components/edit-collateral-kind-modal/edit-collateral-kind-modal.component';
import {TagsManagerComponent} from './management/tags/components/tags-manager/tags-manager.component';
import {CreateTagModalComponent} from './management/tags/components/create-tag-modal/create-tag-modal.component';
import {EditTagModalComponent} from './management/tags/components/edit-tag-modal/edit-tag-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CreateCollateralModalComponent} from './management/collaterals/create-collateral-modal/create-collateral-modal.component';
import {EditCollateralModalComponent} from './management/collaterals/edit-collateral-modal/edit-collateral-modal.component';
import {CollateralsManagerComponent} from './management/collaterals/collaterals-manager/collaterals-manager.component';
import {TagEditComponent} from './management/collaterals/shared/tag-edit/tag-edit.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    MiscellaneousModule,
    NbCardModule,
    NbSpinnerModule,
    NbButtonModule,
    ReactiveFormsModule,
    NbInputModule,
  ],
  declarations: [
    PagesComponent,
    CollateralKindsManagerComponent,
    CreateCollateralKindModalComponent,
    EditCollateralKindModalComponent,
    TagsManagerComponent,
    CreateTagModalComponent,
    EditTagModalComponent,
    CollateralsManagerComponent,
    CreateCollateralModalComponent,
    EditCollateralModalComponent,
    TagEditComponent,
  ],
  entryComponents: [
    CreateCollateralKindModalComponent,
    EditCollateralKindModalComponent,
    CreateTagModalComponent,
    EditTagModalComponent,
  ],
})
export class PagesModule {
}
