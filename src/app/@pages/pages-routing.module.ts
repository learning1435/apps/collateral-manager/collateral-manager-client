import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {PagesComponent} from './pages.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NotFoundComponent} from './miscellaneous/not-found/not-found.component';
import {TagsManagerComponent} from './management/tags/components/tags-manager/tags-manager.component';
import {CollateralKindsManagerComponent} from './management/collateral-kinds/components/collateral-kinds-manager/collateral-kinds-manager.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: '',
      component: DashboardComponent,
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'management/tags',
      component: TagsManagerComponent,
    },
    {
      path: 'management/collateral-kinds',
      component: CollateralKindsManagerComponent,
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
