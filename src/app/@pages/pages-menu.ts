import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Search leads',
    icon: 'search-outline',
    link: '/',
    home: true,
  },
  {
    title: 'My leads',
    icon: 'browser-outline',
    link: '/my-leads',
  },
  {
    title: 'Management',
    group: true,
  },
  {
    title: 'Collaterals',
    icon: 'file-text-outline',
    link: '/management/collaterals',
  },
  {
    title: 'Tags',
    icon: 'pricetags-outline',
    link: '/management/tags',
  },
  {
    title: 'Collateral kinds',
    icon: 'credit-card-outline',
    link: '/management/collateral-kinds',
  },
];
