import {Component} from '@angular/core';
import {MENU_ITEMS} from './pages-menu';
import {NbMenuItem} from '@nebular/theme';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
})
export class PagesComponent {

  public menu: NbMenuItem[] = MENU_ITEMS;
}
